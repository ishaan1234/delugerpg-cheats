{
  const container = {};
  const temp = {};
  let placeholder = '';

  document.querySelectorAll('.container > div').forEach((pokemon) => {
    const id = pokemon.dataset.pkid;
    container[id] = pokemon.outerHTML;
  });

  const getRandomPropertyName = (obj) => {
    // https://stackoverflow.com/a/15106541
    const keys = Object.keys(obj);
    return keys[(keys.length * Math.random()) << 0]; // returns key
  };

  while (Object.keys(container).length > 0) {
    const getKey = getRandomPropertyName(container);
    temp[getKey] = container[getKey];
    delete container[getKey];
  }

  for (const id of Object.keys(temp)) {
    placeholder += temp[id];
  }

  document.querySelector('.container').innerHTML = placeholder;
}
