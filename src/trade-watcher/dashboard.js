const fs = require('fs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

var CronJob = require('cron').CronJob;
var job = new CronJob('2 * * * *', function () {
  console.log('Running.');

  const DATA_FOLDER = '/Users/sage/Downloads';
  let container = {};
  let temp = {};
  let placeholder = '';

  try {
    function getDataHtml() {
      const buffer = fs.readFileSync(DATA_FOLDER + '/0000 Pokemon Data.html');
      const data = buffer.toString();
      return new JSDOM(data);
    }

    const dataHTML = getDataHtml();

    const pokes = dataHTML.window.document.querySelectorAll('.container > div');
    pokes.forEach((poke) => {
      container[poke.id] = poke.outerHTML;
    });
  } catch (e) {}

  fs.readdirSync(DATA_FOLDER).forEach((file) => {
    if (file !== '.DS_Store' && file.includes('wA4Fhe1vUt2g7N')) {
      const buffer = fs.readFileSync(DATA_FOLDER + '/' + file);
      const data = JSON.parse(buffer);
      fs.rmSync(DATA_FOLDER + '/' + file, { force: true }); // 🚧 DELETE OPERATION
      container = {
        ...container,
        ...data
      };
    }
  });

  // Start randomizing of trades
  // The trades will not be shown in a fixed order

  const getRandomPropertyName = (obj) => {
    // https://stackoverflow.com/a/15106541
    const keys = Object.keys(obj);
    return keys[(keys.length * Math.random()) << 0]; // returns key
  };

  while (Object.keys(container).length > 0) {
    const getKey = getRandomPropertyName(container);
    temp[getKey] = container[getKey];
    delete container[getKey];
  }

  // End randomizing of trades

  const count = Object.keys(temp).length;

  for (const id of Object.keys(temp)) {
    placeholder += temp[id];
  }

  const header =
    '<html><head> <meta charset="utf-8"/> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/> <meta name="msapplication-TileImage" content="https://i.dstatic.com/images/win8.png"/> <meta name="msapplication-TileColor" content="#1760fa"/> <meta name="description" content="Trade Search"/> <script src="/cdn-cgi/apps/head/w_yXLKNQDfZ7AbWIl8B0nh5WruM.js"></script> <link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.delugerpg.com/trade/lookup"/> <meta name="referrer" content="origin"/> <title>data</title> <link rel="shortcut icon" href="/apple-touch-icon.png"/> <link rel="apple-touch-icon" href="/apple-touch-icon.png"/> <meta name="msapplication-config" content="/browserconfig.xml"/> <meta name="theme-color" content="#0039af"/> <meta property="og:title" content="Trade Search"/> <meta property="og:site_name" content="DelugeRPG"/> <meta property="og:image" content="https://i.dstatic.com/images/win8.png"/> <meta property="twitter:site" content="@DelugeRPG"/> <link rel="shortcut icon" href="https://i.dstatic.com/favicon.ico"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-out.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-in.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-bootstrap.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/deps/AmaranJS/css/amaran.min.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-ddahead.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-pokemon.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-trade.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-dex.css"/> <link rel="stylesheet" href="https://assets.delugerpg.com/css20817/l-d-pagination.css"/><!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" integrity="sha512-UDJtJXfzfsiPPgnI5S1000FPLBHMhvzAMX15I+qG2E2OAzC9P1JzUwJOfnypXiOH7MRPaqzhPbBGDNNj7zBfoA==" crossorigin="anonymous" referrerpolicy="no-referrer" ></script ><![endif]--><!--[if IE]><script src="https://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js" integrity="sha512-0mXreXYrXoy9laHoypsAOjuSCqh57vY+kIdE46k8Hw0yRY1EhJyHWUEAqfHOTrPkBpsbO39/ZPw5HITv8mLVOA==" crossorigin="anonymous" referrerpolicy="no-referrer" ></script ><![endif]--></head>';

  const body =
    '<body><h2 style="text-align: center;margin: 10px;font-size: 18px;font-family: Impact;">' +
    count +
    '</h2><div id="monboxes" style="width: 900px;margin: 0 auto;min-width: 780px;"><div class="row head"><div class="data">&nbsp;</div><div class="name">Pokemon</div><div class="info">Level / Exp / Type</div><div class="attk">Moves</div><div class="opts"></div></div><div class="container" style="max-height:none">';

  const pokeballClose =
    'document.querySelectorAll(".pkd_hon").forEach(function(pokeball){pokeball.textContent="❌";pokeball.style.cursor="pointer";pokeball.addEventListener("click",function(el){el.target.closest(".row").remove();document.querySelector("body > h2").textContent-=1},true)});';
  // ^ Close unwanted pokes and save to data html
  const footer = '</div><script>' + pokeballClose + '</script></body></html>';

  placeholder = header + body + placeholder + footer;

  fs.writeFileSync(DATA_FOLDER + '/0000 Pokemon Data.html', placeholder);

  // var start =
  //   process.platform == 'darwin'
  //     ? 'open'
  //     : process.platform == 'win32'
  //     ? 'start'
  //     : 'xdg-open';
  // require('child_process').exec(
  //   start + ' ' + DATA_FOLDER + '/0000 Pokemon Data.html'
  // );
});

job.start();
