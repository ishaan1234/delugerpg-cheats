// ==UserScript==
// @name         Send Non-Evo Pokemon to Trade
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatically send non-evolving Pokémon to trade on delugerpg.com
// @author       You
// @match        https://www.delugerpg.com/pokemon/page/*
// @match        https://www.delugerpg.com/pokemon/search/%2*
// @match        https://www.delugerpg.com/pokemon*
// @match        https://www.delugerpg.com/pokemon/trade/*
// @match        https://www.delugerpg.com/pokemon/order/dex_asc/page/*
// @match        https://www.delugerpg.com/pokemon/order/dex_desc/page/*
// @match        https://www.delugerpg.com/pokemon/order/name_asc/page/*
// @match        https://www.delugerpg.com/pokemon/evolve/*
// @match        https://www.delugerpg.com/pokemon/search/%40legends
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

/*

*/

const DELAY_MULTIPLIER = 2000;

(function cloze() {
  if (document.location.href === 'https://www.delugerpg.com/pokemon') {
    window.close();
  }
  if (
    document.location.href.includes('https://www.delugerpg.com/pokemon/evolve/')
  ) {
    const evolved = document.querySelector('img.imgtolevel').src;
    if (evolved === 'https://i.dstatic.com/images/pokeball-y.png') {
      document.location.href =
        'https://www.delugerpg.com/pokemon/trade/' +
        document.location.pathname.split('/').reverse()[0];
    } else {
      window.close();
    }
  }
})();

if (document.querySelector('.notify_done')) {
  const txt = document.querySelector('.notify_done').textContent;
  txt.includes('successfully put up for trade') ? window.close() : null;
} else if (document.querySelector('.notify_error')) {
  const txt = document.querySelector('.notify_error').textContent;
  txt.includes('Cannot put this poke up for trade') ? window.close() : null;
}

async function clickButton(selector, value) {
  setTimeout(async () => {
    const res = await isLoading(0);
    if (!res) {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && !altBtn) {
          btn.click();
        }
      }
    }
  }, 500 * DELAY_MULTIPLIER);
}

function send() {
  let delay = 4000;
  let counter = 0;
  let temp = 1;
  const pokes = document.querySelectorAll('.container > *');
  pokes.forEach((poke) => {
    const hasEvolve = !!poke.querySelector('.evolve');
    const iid = poke.id.substring(4);
    if (!hasEvolve) {
      counter += 1;
      setTimeout(() => {
        const aaa = poke.querySelector('.trade').href;
        window.open(aaa);
        temp += 1;
      }, delay);
      delay += 1000;
    } else {
      counter += 1;
      setTimeout(() => {
        const aaa = poke.querySelector('.evolve').href;
        window.open(aaa);
        temp += 1;
      }, delay);
      delay += 1000;
    }
  });
}

function isLoading(timeout) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(false);
    }, timeout);
  });
}

function main() {
  const nextPageButton = document.querySelector(
    '.pagenum.pagebutton i.fa-chevron-right'
  );
  const hasNextButton = !!nextPageButton;

  if (runOnceOnLastPageFlag) {
    send();
    setTimeout(() => {
      try {
        nextPageButton.click();
      } catch (e) {
        runOnceOnLastPageFlag = false;
        clearInterval(looper);
      }
    }, 2000);
  } else if (!hasNextButton && runOnceOnLastPageFlag) {
    runOnceOnLastPageFlag = false;
    main();
    clearInterval(looper);
  }
}

let runOnceOnLastPageFlag = true;
const looper = setInterval(main, 3000);
