// ==UserScript==
// @name         Find Level Pokemon
// @version      2024-04-14
// @description  Find a pokemon with a certain level so that you can grind
// @namespace    https://gitlab.com/rosavant/delugerpg-hacks/delugerpg-cheats
// @author       rosavant
// @match        https://*.delugerpg.com/map/*
// @updateURL    https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/find-level-pokemon.user.js
// @downloadURL  https://gitlab.com/rosavant/delugerpg-bots/raw/master/src/pokemon-catcher/find-level-pokemon.user.js
// @grant        none
// ==/UserScript==

const DELAY_MULTIPLIER = weightedRandom(1.5, undefined, 3.5);
const level = prompt('What level? (seperated by space)', 8).split(' ');
const SUBDOMAIN = document.location.host.split('.')[0];
const WEBSITE = `https://${SUBDOMAIN}.delugerpg.com`;
let desire = false;

/**
 * From https://stackoverflow.com/a/46774731
 * Generates random time with minimum having the most weight
 * @param {number} min lets say I want 0.5 as a multiplier, consider x ,so I'll do x * 10 theb min = (x * 10) - 1
 * @param {number} max lets say I want it to be 5, consider y, so I'll do y * 10 then max = (y*10) - min
 * @param {number} fixed should always be one make range by remind one from lower range and then subtracting lower range from max like explained above
 * @returns {number} time
 */
function weightedRandom(min, fixed = 1, max) {
  return (
    (min * 10 -
      1 +
      Math.round(
        (max * 10 - (min * 10 - 1)) /
          (Math.random() * (max * 10 - (min * 10 - 1)) + fixed)
      )) /
    10
  );
}

function gotoMp() {
  // prettier-ignore
  const maps = ['/map/overworld1', '/map/overworld2', '/map/overworld3','/map/overworld4','/map/overworld5','/map/overworld6','/map/overworld7','/map/overworld8','/map/overworld9','/map/overworld10','/map/overworld11','/map/overworld12','/map/overworld13','/map/overworld14','/map/overworld15','/map/overworld16','/map/overworld17','/map/overworld18','/map/overworld19','/map/overworld20','/map/overworld21','/map/overworld22','/map/overworld23','/map/overworld24','/map/overworld25','/map/snowcave1','/map/snowcave2','/map/snowcave3','/map/snowcave4','/map/volcano1','/map/volcano2','/map/volcano3','/map/volcano4','/map/pkmntower1','/map/pkmntower2','/map/pkmntower3','/map/powerplant1','/map/powerplant2','/map/powerplant3','/map/pkmnmansion1','/map/pkmnmansion2','/map/pkmnmansion3','/map/pkmnmansion4','/map/rockcave1','/map/rockcave2','/map/rockcave3','/map/rockcave4'];
  const gotoMap = maps[Math.floor(Math.random() * maps.length)];
  const gotoMapURL = `${WEBSITE}${gotoMap}`;
  document.location.href = gotoMapURL;
}

(function () {
  let flag = null;

  // console.clear();

  function startTimer(duration, display) {
    let start = Date.now();
    let diff;
    let minutes;
    let seconds;
    function timer() {
      // get the number of seconds that have elapsed since
      // startTimer() was called
      diff = duration - (((Date.now() - start) / 1000) | 0);

      // does the same job as parseInt truncates the float
      minutes = (diff / 60) | 0;
      seconds = diff % 60 | 0;

      minutes = minutes < 10 ? '0' + minutes : minutes;
      seconds = seconds < 10 ? '0' + seconds : seconds;

      if (minutes == 0) {
        gotoMp();
      }

      if (seconds == 0) {
        // console.clear();
        // console.log(minutes + ':' + seconds);
      }

      // console.log(minutes + ':' + seconds);

      if (diff <= 0) {
        // add one second so that the count down starts at the full duration
        // example 05:00 not 04:59
        start = Date.now() + 1000;
      }
    }
    // we don't want to wait a full second before the timer starts
    timer();
    setInterval(timer, 1000);
  }

  (function () {
    let mins = 60 * 10;
    startTimer(mins);
  })();

  setTimeout(
    () => {
      flag = true;
      setTimeout(
        () => {
          if (flag) {
            gotoMp();
          }
        },
        10 * 60 * 1000
      );
    },
    10 * 60 * 1000
  );

  async function clickButton(selector, value) {
    setTimeout(async () => {
      const potentialButtons = document.querySelectorAll(selector);
      for (const btn of potentialButtons) {
        const val = btn.value.trim();
        const altBtn = btn.style.length;
        if (val === value && altBtn === 0) {
          btn.click();
        }
      }
    }, 500);
  }

  const moveNorth = document.querySelector('div#dr-n');
  const moveNorthWest = document.querySelector('div#dr-nw');
  const moveNorthEast = document.querySelector('div#dr-ne');
  const moveEast = document.querySelector('div#dr-e');
  const moveWest = document.querySelector('div#dr-w');
  const moveSouth = document.querySelector('div#dr-s');
  const moveSouthWest = document.querySelector('div#dr-sw');
  const moveSouthEast = document.querySelector('div#dr-se');

  const moveVector = [
    moveNorth,
    moveNorthWest,
    moveNorthEast,
    moveEast,
    moveWest,
    moveSouth,
    moveSouthWest,
    moveSouthEast
  ];

  function move() {
    const directon = () => {
      const vector = moveVector[Math.floor(Math.random() * moveVector.length)];
      const isVectorDisabled = vector.classList.contains('m-disable') | false;
      isVectorDisabled ? directon() : null;
      return vector;
    };

    directon().click();
  }

  (function () {
    const main = setInterval(
      () => {
        if (document.querySelector('div#hp')) {
          const pokemonLevel = document
            .querySelector('div#hp')
            .textContent.trim()
            .match(/Level:\s?(\d+)/)[1];
          level.includes(pokemonLevel) ? (desire = true) : null;
        }

        if (desire) {
          clearInterval(main);
          clickButton('.btn-catch-action', 'Try to Catch It');
        } else {
          setTimeout(move, 200);
        }
      },
      Math.floor(
        Math.random() * (1000 * DELAY_MULTIPLIER + 200 * DELAY_MULTIPLIER) -
          200 * DELAY_MULTIPLIER
      )
    );
  })();
})();
