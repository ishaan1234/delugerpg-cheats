// ==UserScript==
// @name         Close Empty Trades FAIR
// @version      0.2
// @description  Simply find the pokemons the user to whole dex is compared doesnt have
// @match        https://*.delugerpg.com/trade/lookup/pokemon/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// ==/UserScript==

function closeEmpty() {
  const pokemonsLength = document.querySelectorAll(
    '#monboxes > .container > *'
  ).length;
  if (!pokemonsLength) window.close();
}

closeEmpty();

function openAllTrades() {
  if (
    !document.location.href.includes('/page') &&
    document.querySelectorAll('.pagenum').length > 0
  ) {
    const pages = document.querySelectorAll('.pagenum');
    const lastPage = pages
      ? Number(pages[pages.length - 2].textContent.trim())
      : null;
    if (lastPage) {
      // url = https://www.delugerpg.com/trade/lookup/pokemon/Leafeon/class/retro/page/1
      for (let i = 2; i <= lastPage; i++) {
        window.open(document.location.href + `/page/${i}`, '_blank');
      }
    }
  }
}

openAllTrades();

function sanitizeBattles(battles) {
  let btl = 0;
  if (battles.includes('k')) {
    btl = parseInt(battles) * 1000;
  } else {
    btl = parseInt(battles.replaceAll(',', ''));
  }
  return btl;
}

function sanitizeExp(exp) {
  let ex = 0;
  if (exp.includes('B')) {
    ex = parseInt(exp) * 1000000000;
  } else if (exp.includes('M')) {
    ex = parseInt(exp) * 1000000;
  } else {
    ex = parseInt(exp.replaceAll(',', ''));
  }
  return ex;
}

const getDetails = (id) => {
  const url = 'https://www.delugerpg.com/ajax/profile/uid/' + id;
  return new Promise(async (resolve) => {
    fetch(url)
      .then((response) => response.json())
      .then((data) =>
        resolve({
          banned: data.status ? true : false, // banned user have a "status" property. If it exists then that means the person is banned
          id: Number(data.profileid),
          user: data.username,
          battles:
            sanitizeBattles(data.bwins) +
            (data.blosses ? sanitizeBattles(data.blosses) : 0),
          exp: sanitizeExp(data.ttlexp),
          alt: !!Number(data.spare),
          join: removeTimezoneOffset(new Date(data.joindate)).toISOString(),
          online: removeTimezoneOffset(new Date(data.lastonline)).toISOString()
        })
      );
  });
};

function removeTimezoneOffset(date) {
  // new Date(new Date("19 Oct 2022").getTime() - new Date("19 Oct 2022").getTimezoneOffset() * 60000).toISOString()
  let userTimezoneOffset = date.getTimezoneOffset() * 60000;
  if (userTimezoneOffset >= 0) {
    return new Date(date.getTime() - userTimezoneOffset);
  }
  return new Date(date.getTime() + userTimezoneOffset);
}

function isTrained(pokeDOM) {
  const poke = pokeDOM.querySelectorAll('.info > div');
  const level = parseInt(poke[0].textContent.trim().match(/Level:\s(\d+)$/)[1]);
  const lastThree = parseInt(
    poke[1].textContent
      .trim()
      .match(/Exp:\s([\d\,]+)/)[1]
      .replaceAll(',', '')
      .slice(-3)
  ); // should be 0 as parseInt 000 = 0

  if (level === 100 || lastThree !== 0) return true;

  return false;
}

async function noobCheck() {
  const pokemons = document.querySelectorAll('#monboxes > .container > *');
  const notSnipeable = await JSON.parse(
    GM_getValue('not_snipeable', JSON.stringify({}))
  );
  const snipeable = await JSON.parse(
    GM_getValue('snipeable', JSON.stringify({}))
  );
  await GM_getValue('firstTemp', new Date().toISOString());
  await GM_setValue('lastTemp', new Date().toISOString());
  const blacklist = await JSON.parse(
    GM_getValue('blacklist', JSON.stringify({}))
  ); // active and noob
  pokemons.forEach(async (poke) => {
    const user = poke.querySelector('.username').textContent.trim();
    const userId = poke.querySelector('.username').id.substring(2);
    if (notSnipeable[userId] || blacklist[userId] || Number(userId) === 0) {
      // We will remove the pokemon if it exists in pro, or blacklist or has id equal to 0 (this is for cases where the user has exceeded maximum pokemons)
      poke.remove();
    } else if (
      !notSnipeable[userId] &&
      !blacklist[userId] &&
      !snipeable[userId]
    ) {
      // If the user is not in pro, blacklist, noob (prevent rechecking)

      const snipe = await isSnipeable(userId, poke); // fetch new data of user

      if (!snipe) {
        notSnipeable[userId] = user;
        await GM_setValue('not_snipeable', JSON.stringify(notSnipeable));
        poke.remove();
      } else {
        snipeable[userId] = user;
        await GM_setValue('snipeable', JSON.stringify(snipeable));
      }
    }
    closeEmpty();
  });
}

async function isSnipeable(id, dom) {
  const res = await getDetails(id);
  const X_DAYS_AGO = 3;
  const Y_DAYS_AGO = 60;
  let snipe = true;
  let newb = true;
  const lastOnlineMark = new Date(
    new Date().setDate(new Date().getDate() - X_DAYS_AGO)
  );
  const accCreationMark = new Date(
    new Date().setDate(new Date().getDate() - Y_DAYS_AGO)
  );
  // Check if banned
  if (res.banned) {
    snipe = false;
  }
  // Check battles
  if (res.battles > 3000) {
    snipe = false;
  }
  // // Check exp
  if (res.exp > 300 * 1000000) {
    snipe = false;
  }
  // // Check if alt
  if (res.alt) {
    snipe = false;
  }
  // Check account last online
  if (new Date(res.online).getTime() < lastOnlineMark.getTime()) {
    snipe = false;
  }
  // Check account creation date
  // if (new Date(res.join).getTime() < accCreationMark.getTime()) {
  //   snipe = false;
  // }

  // if (isTrained(dom)) {
  //   snipe = false;
  // }

  return snipe;
}

noobCheck();
