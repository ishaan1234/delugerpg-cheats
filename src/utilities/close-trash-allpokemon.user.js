// ==UserScript==
// @name         Close Trash
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/allpokemon/user/*/filter/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        GM_openInTab
// @run-at       document-end
// ==/UserScript==

(function () {
  'use strict';

  // Cache DOM elements to avoid redundant lookups.
  const notifyError = document.querySelector('.notify_error');
  const viewPokes = document.querySelectorAll('#viewpokes > *');

  /**
   * Handles the '.notify_error' element logic.
   * Closes the window and opens a new tab if the error message matches 'No such user.'.
   */
  function handleNotifyError() {
    if (notifyError.textContent.trim() === 'No such user.') {
      window.close();
      GM_openInTab(
        'https://www.google.com/search?client=firefox-b-d&q=NO%20USER'
      );
    }
  }

  /**
   * Checks if the given element has all the required stats.
   * @param {Element} element - The DOM element to check.
   * @returns {boolean} True if all stats are present, false otherwise.
   */
  function hasAllStats(element) {
    return ['i.sbtn-spe', 'i.sbtn-atk', 'i.sbtn-def'].every((selector) =>
      element.querySelector(selector)
    );
  }

  /**
   * Determines if a given element matches the criteria for a desired legendary Pokemon.
   * @param {Element} element - The DOM element to check.
   * @returns {boolean} True if it's a desired legendary, false otherwise.
   */
  function isDesiredLegend(element) {
    // prettier-ignore
    const legends = ['Arceus', 'Articuno', 'Azelf', 'Blacephalon', 'Buzzwole', 'Calyrex', 'Celebi', 'Celesteela', 'Cobalion', 'Cosmoem', 'Cosmog', 'Cresselia', 'Darkrai', 'Deoxys', 'Dialga', 'Diancie', 'Enamorus', 'Entei', 'Eternatus', 'G-Max Melmetal', 'G-Max Urshifu', 'Genesect', 'Giratina', 'Glastrier', 'Groudon', 'Guzzlord', 'Heatran', 'Ho-oh', 'Hoopa', 'Jirachi', 'Kartana', 'Keldeo', 'Kubfu', 'Kyogre', 'Kyurem', 'Landorus', 'Latias', 'Latios', 'Lugia', 'Lunala', 'Magearna', 'Manaphy', 'Marshadow', 'Mega Diancie', 'Mega Latias', 'Mega Latios', 'Mega Mewtwo X', 'Mega Mewtwo Y', 'Mega Rayquaza', 'Melmetal', 'Meloetta', 'Meltan', 'Mesprit', 'Mew', 'Mewtwo', 'Moltres', 'Naganadel', 'Necrozma', 'Nihilego', 'Palkia', 'Pheromosa', 'Phione', 'Poipole', 'Primal Groudon', 'Primal Kyogre', 'Raikou', 'Rayquaza', 'Regice', 'Regidrago', 'Regieleki', 'Regigigas', 'Regirock', 'Registeel', 'Reshiram', 'Shaymin', 'Silvally', 'Solgaleo', 'Spectrier', 'Stakataka', 'Suicune', 'Tapu Bulu', 'Tapu Fini', 'Tapu Koko', 'Tapu Lele', 'Terrakion', 'Thundurus', 'Tornadus', 'Type: Null', 'Urshifu', 'Uxie', 'Victini', 'Virizion', 'Volcanion', 'Xerneas', 'Xurkitree', 'Yveltal', 'Zacian', 'Zamazenta', 'Zapdos', 'Zarude', 'Zekrom', 'Zeraora', 'Zygarde', ];
    const wantedClass = ['Shiny', 'Retro', 'Chrome'];
    const name = element.querySelector('.vpname > h4').textContent.trim();
    const fname = name.split(' ')[0];
    return legends.some(
      (legend) =>
        name.includes(legend) &&
        wantedClass.includes(fname) &&
        element.querySelector('i.sbtn-spe')
    );
  }

  /**
   * Processes the list of Pokemon.
   * Removes Pokemon that do not have all stats and are not desired legendaries.
   * Closes the window if no Pokemon remain after processing.
   */
  function processPokemons() {
    const pokemons = document.querySelectorAll('#viewpokes > div');
    pokemons.forEach((pokemon) => {
      if (!hasAllStats(pokemon) && !isDesiredLegend(pokemon)) {
        pokemon.remove();
      }
    });
    if (!document.querySelectorAll('#viewpokes > *').length) {
      window.close();
    }
  }

  // If there's a notification error, handle it.
  if (notifyError) {
    handleNotifyError();
  }

  // If there are no Pokemon to view, close the window.
  if (!viewPokes.length) {
    window.close();
  } else {
    processPokemons();
  }
})();
