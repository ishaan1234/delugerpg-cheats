// prettier-ignore
const rates = [
  15, 25, 75, 75, 75, 75, 'SET', 125, 'SET', 'SET', 125, 35
];

const css =
  'clear:right;font-family: impact;font-size: 2rem;padding-top: 45px;padding-right: 19px;/*! background-color: #3a50d9; */color: red;/*! font: italic bold 100px Georgia, Serif; */text-shadow: -4px 3px 0 rgb(0, 0, 0, 0.15);';

document.querySelectorAll('.viewpoketable .center').forEach((el, index) => {
  const rate = rates[index];
  el.innerHTML = typeof rate === 'number' ? `${rate}M` : rate;
  el.style = css;
});

const sum = rates.reduce((a, b) => a + b, 0);
