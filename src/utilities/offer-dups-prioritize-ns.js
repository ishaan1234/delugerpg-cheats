// Use on multiselect trade page

let container = {};
let totalDuplicatePokemons = 0;
let totalDups = 0;
let clickCounter = 0;
const TO_SELECT = 200; // keep it more than the selection to acc for errors
const SELECT_LIMIT = 300;
const DELAY = 1000;

const removeCountOnes = (obj) => {
  const newObj = {};
  const pokemons = Object.keys(obj);
  pokemons.forEach((pokemon) => {
    if (obj[pokemon].total > 1) {
      newObj[pokemon] = obj[pokemon];
    }
  });
  return newObj;
};

const checkForExp = (element) => {
  const exp = Number(
    element
      .querySelector('.info')
      .textContent.trim()
      .match(/Exp:\s?([\d,]+)/)[1]
      .replaceAll(',', '')
  );
  if (exp >= 1000000) {
    return true;
  } else {
    return false;
  }
};

const findDuplicatePokemons = () => {
  if (totalDups > TO_SELECT) return TO_SELECT;
  const getPokemons = document.querySelectorAll('.container > .row');
  getPokemons.forEach((pokemon) => {
    // totalPokemons += 1;
    const speEl = !!pokemon.querySelector('i.sbtn-spe');
    const atkEl = !!pokemon.querySelector('i.sbtn-atk');
    const defEl = !!pokemon.querySelector('i.sbtn-def');
    const hasAtk = !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
    const hasDef = !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
    const hasSpe = !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
    const hasSpeAtk = speEl && atkEl && !defEl;
    const hasSpeDef = speEl && defEl && !atkEl;
    const hasAtkDef = hasAtk && defEl && !speEl;
    const hasExp = checkForExp(pokemon);
    const name = pokemon.querySelector('div.name').dataset.name.trim();
    if (Object.keys(container).includes(name)) {
      // pokemon.click();
      totalDups += 1;
      container[name].total += 1;
      if (hasExp) {
        container[name].segmented.exp += 1;
      } else if (hasAtk) {
        container[name].segmented.atk += 1;
      } else if (hasDef) {
        container[name].segmented.def += 1;
      } else if (hasSpe) {
        container[name].segmented.spe += 1;
      } else if (hasSpeAtk) {
        container[name].segmented.speAtk += 1;
      } else if (hasSpeDef) {
        container[name].segmented.speDef += 1;
      } else if (hasAtkDef) {
        container[name].segmented.atkDef += 1;
      } else if (hasExp) {
        container[name].segmented.exp += 1;
      } else {
        container[name].segmented.normie += 1;
      }
    } else {
      container[name] = {
        total: 1,
        segmented: {
          normie: 0,
          exp: 0,
          spe: 0,
          atk: 0,
          def: 0,
          speAtk: 0,
          speDef: 0,
          atkDef: 0
        }
      };
      if (hasExp) {
        container[name].segmented.exp += 1;
      } else if (hasAtk) {
        container[name].segmented.atk += 1;
      } else if (hasDef) {
        container[name].segmented.def += 1;
      } else if (hasSpe) {
        container[name].segmented.spe += 1;
      } else if (hasSpeAtk) {
        container[name].segmented.speAtk += 1;
      } else if (hasSpeDef) {
        container[name].segmented.speDef += 1;
      } else if (hasAtkDef) {
        container[name].segmented.atkDef += 1;
      } else if (hasExp) {
        container[name].segmented.exp += 1;
      } else {
        container[name].segmented.normie += 1;
      }
    }
  });
};

let TRIGGER = false;

const doDuplicatePokemons = (checklist) => {
  const getPokemons = document.querySelectorAll('.container > .row');
  getPokemons.forEach((pokemon) => {
    if (clickCounter === SELECT_LIMIT) {
      clearInterval(goingBackward);
      throw new Error('selected 100');
    }
    const speEl = !!pokemon.querySelector('i.sbtn-spe');
    const atkEl = !!pokemon.querySelector('i.sbtn-atk');
    const defEl = !!pokemon.querySelector('i.sbtn-def');
    const hasAtk = !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
    const hasDef = !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
    const hasSpe = !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
    const hasSpeAtk = speEl && atkEl && !defEl;
    const hasSpeDef = speEl && defEl && !atkEl;
    const hasAtkDef = hasAtk && defEl && !speEl;
    const hasNoStat = !speEl && !atkEl && !defEl;
    const hasExp = checkForExp(pokemon);
    const name = pokemon.querySelector('div.name').dataset.name.trim();
    if (Object.keys(checklist).includes(name) && checklist[name].total > 1) {
      // console.log(checklist[name]);
      if (hasNoStat && !hasExp && checklist[name].segmented.normie > 0) {
        pokemon.click();
        clickCounter += 1;
        checklist[name].segmented.normie -= 1;
        checklist[name].total -= 1;
      } else if (
        checklist[name].segmented.normie === 0 &&
        hasAtk &&
        !hasExp &&
        checklist[name].segmented.atk > 0
      ) {
        pokemon.click();
        clickCounter += 1;
        checklist[name].segmented.atk -= 1;
        checklist[name].total -= 1;
      } else if (
        checklist[name].segmented.normie === 0 &&
        hasDef &&
        !hasExp &&
        checklist[name].segmented.def > 0
      ) {
        pokemon.click();
        clickCounter += 1;
        checklist[name].segmented.def -= 1;
        checklist[name].total -= 1;
      } else if (
        checklist[name].segmented.normie === 0 &&
        checklist[name].segmented.atk === 0 &&
        checklist[name].segmented.def === 0 &&
        hasAtkDef &&
        !hasExp &&
        checklist[name].segmented.atkDef > 0
      ) {
        pokemon.click();
        clickCounter += 1;
        checklist[name].segmented.atkDef -= 1;
        checklist[name].total -= 1;
      }
    }
  });
  return checklist;
};

const forward = (page = 999) => {
  try {
    if (document.querySelector('.modal-backdrop.fade')) {
      document.querySelector('.modal-backdrop.fade').remove();
    }
    const currentPage = document.querySelector('#pagesel')
      ? document.querySelector('#pagesel').textContent.trim()
      : null;
    if (
      document.querySelector('.pagenum.pagebutton i.fa-chevron-right') &&
      currentPage < page &&
      !TRIGGER
    ) {
      const ayy = findDuplicatePokemons();
      if (ayy === TO_SELECT) TRIGGER = true;
      document.querySelector('.pagenum.pagebutton i.fa-chevron-right').click();
      ////
      // container = removeCountOnes(container);
      ////
    } else {
      clearInterval(goingForward);
      findDuplicatePokemons();
      console.clear();
      let res = removeCountOnes(container);
      console.log(`Original count: ${Object.keys(container).length}`);
      console.log(container);
      console.log(`Truncated uniques: ${Object.keys(res).length}`);
      // console.log(res);
      // return res;
      // const goingBackward = setInterval(backward, 1000, res);
      console.log(
        `Going back from pg. ${document
          .querySelector('#pagesel')
          .textContent.trim()}`
      );
      const goingBackward = setInterval((page = 1) => {
        try {
          if (document.querySelector('.modal-backdrop.fade')) {
            document.querySelector('.modal-backdrop.fade').remove();
          }
          const currentPage = document.querySelector('#pagesel')
            ? document.querySelector('#pagesel').textContent.trim()
            : null;
          if (
            document.querySelector('.pagenum.pagebutton i.fa-chevron-left') &&
            currentPage > page
          ) {
            res = doDuplicatePokemons(res);
            document
              .querySelector('.pagenum.pagebutton i.fa-chevron-left')
              .click();
          } else {
            clearInterval(goingBackward);
            res = doDuplicatePokemons(res);
          }
        } catch (e) {
          clearInterval(goingBackward);
          console.log(e);
        }
      }, DELAY);
    }
  } catch (e) {
    clearInterval(goingForward);
    container = {};
    console.log(e);
  }
};

const goingForward = setInterval(forward, DELAY);

// let stuff = temp0;

// back

// const goingBackward = setInterval(backward, 1000, stuff);

//   const main = () => {};
