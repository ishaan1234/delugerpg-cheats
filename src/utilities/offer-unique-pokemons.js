/* eslint-disable userscripts/filename-user */
/* eslint-disable userscripts/no-invalid-metadata */
/* eslint-disable userscripts/filename-user */
{
  const selectedMons = {};
  let clickCounter = 0;
  const TO_SELECT = prompt('how many', 100); // keep it more than the selection to acc for errors
  const SELECT_LIMIT = TO_SELECT;
  const DELAY = 1200;

  const checkForExp = (element) => {
    const exp = Number(
      element
        .querySelector('.info')
        .textContent.trim()
        .match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (exp >= 1000000) {
      return true;
    } else {
      return false;
    }
  };

  const checkForHundredLevel = (element) => {
    const level = Number(
      element
        .querySelector('.info')
        .textContent.trim()
        .match(/Level:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (level === 100) {
      return true;
    } else {
      return false;
    }
  };

  const selectThisClass = (name, spirit) => {
    const SPIRIT_LIST = [
      'Normal',
      'Shiny',
      'Metallic',
      'Ghostly',
      'Dark',
      'Shadow',
      'Mirage',
      'Chrome',
      'Negative',
      'Retro'
    ];

    const firstWord = name.split(' ')[0];

    if (!SPIRIT_LIST.includes(firstWord)) name = 'Normal ' + name;

    if (Array.isArray(spirit) && !spirit.includes(name.split(' ')[0]))
      return false;
    return true;
  };

  const dontOfferThese = (name, dontOffer) => {
    let flag = true;
    for (const blacklist of dontOffer) {
      console.log(
        name,
        dontOffer,
        blacklist,
        name.toLowerCase().includes(blacklist.toLowerCase())
      );
      if (name.toLowerCase().includes(blacklist.toLowerCase())) {
        flag = false;
      }
    }
    return flag;
  };

  // prettier-ignore
  const normalLegends=["Articuno","Zapdos","Moltres","Mewtwo","Mega Mewtwo X","Mega Mewtwo Y","Raikou","Entei","Suicune","Lugia","Ho-oh","Regirock","Regice","Registeel","Latias","Mega Latias","Latios","Mega Latios","Kyogre","Primal Kyogre","Groudon","Primal Groudon","Rayquaza","Mega Rayquaza","Uxie","Mesprit","Azelf","Dialga","Palkia","Heatran","Regigigas","Giratina","Cresselia","Cobalion","Terrakion","Virizion","Tornadus","Thundurus","Reshiram","Zekrom","Landorus","Kyurem","Xerneas","Yveltal","Zygarde","Type: Null","Silvally","Tapu Koko","Tapu Lele","Tapu Bulu","Tapu Fini","Cosmog","Cosmoem","Solgaleo","Lunala","Necrozma","Zacian","Zamazenta","Eternatus","Kubfu","Urshifu","G-Max Urshifu","Regieleki","Regidrago","Glastrier","Spectrier","Calyrex","Enamorus","Wo-Chien","Chien-Pao","Ting-Lu","Chi-Yu","Koraidon","Miraidon"];
  // prettier-ignore
  const mythicalLegends=["Mew","Celebi","Jirachi","Deoxys","Phione","Manaphy","Darkrai","Shaymin","Arceus","Victini","Keldeo","Meloetta","Genesect","Diancie","Mega Diancie","Hoopa","Volcanion","Magearna","Marshadow","Zeraora","Meltan","Melmetal","G-Max Melmetal","Zarude",]
  // prettier-ignore
  const ultrabeastLegends=["Nihilego","Buzzwole","Pheromosa","Xurkitree","Celesteela","Kartana","Guzzlord","Poipole","Naganadel","Stakataka","Blacephalon",];
  // prettier-ignore
  const paradoxLegends=["Great Tusk","Scream Tail","Brute Bonnet","Flutter Mane","Slither Wing","Sandy Shocks","Iron Treads","Iron Bundle","Iron Hands","Iron Jugulis","Iron Moth","Iron Thorns","Roaring Moon","Iron Valiant","Koraidon","Miraidon","Walking Wake","Iron Leaves",];
  // text below has to be in 1 line
  // prettier-ignore
  const legends = [...normalLegends, ...mythicalLegends
    // ...ultrabeastLegends,
    // ...paradoxLegends,
  ];

  let TRIGGER = false;

  const doDuplicatePokemons = () => {
    const getPokemons = document.querySelectorAll('.container > .row');
    getPokemons.forEach((pokemon) => {
      if (clickCounter >= SELECT_LIMIT) {
        clearInterval(goingForward);
        throw new Error('selected 100');
      }
      if (document.querySelector('.modal-backdrop.fade')) {
        document.querySelector('.modal-backdrop.fade').remove();
      }

      const speEl = !!pokemon.querySelector('i.sbtn-spe');
      const atkEl = !!pokemon.querySelector('i.sbtn-atk');
      const defEl = !!pokemon.querySelector('i.sbtn-def');
      const hasExp = checkForExp(pokemon);
      const isLevelHundred = checkForHundredLevel(pokemon);
      const hasNoStat = !speEl && !atkEl && !defEl;
      const hasThreeStat = speEl && atkEl && defEl;
      const hasStat = speEl || atkEl || defEl;
      const doesntHave = !!pokemon.querySelector('i.not');
      const name = pokemon.querySelector('.name').textContent.trim();
      const isLegend = () => {
        for (const legend of legends) {
          if (name.toLowerCase().includes(legend.toLowerCase())) return true;
        }
        return false;
      };
      const isRetroLegend = () => {
        const wantedSpirites = ['Retro']; // probability: 5 1 5 5 5 10 10 10 10
        for (const legend of legends) {
          if (
            name.toLowerCase().includes(legend.toLowerCase()) &&
            name.toLowerCase().includes(wantedSpirites[0].toLowerCase())
          )
            return true;
        }
        return false;
      };

      const isSelectedMultiple = (selectedXtimes = 0, poke) => {
        if (selectedXtimes >= poke) return false;
        return true;
      };

      if (selectedMons.hasOwnProperty(name)) selectedMons[name] += 1;
      else selectedMons[name] = 1;

      // ********** CONDITION ***********
      // prettier-ignore
      // const condition = doesntHave && hasNoStat && !hasExp; // Most prefered
      // prettier-ignore
      // const condition = doesntHave && !hasExp && !speEl && !(atkEl && defEl); // OK
      // prettier-ignore
      // const condition = doesntHave && !hasExp && !isLegend(name) && !selectedMons[name]; // Least if the count is not satisfactory
      // prettier-ignore
      // const condition = doesntHave && !hasExp && !speEl && !selectedMons[name]; // Least if the count is not satisfactory
      // prettier-ignore
      // const condition = doesntHave && !hasExp && hasNoStat && !hasThreeStat && !isLevelHundred && isLegend() && !isRetroLegend() && !selectedMons[name]; // Custom

      // this logic is used to change by hand
      const condition =
        !hasThreeStat
        && !speEl
        && !((atkEl && speEl)|| (speEl && defEl))
        && !(atkEl && defEl)
        && isLegend()
        && !selectThisClass(name, ['Shiny', 'Retro', 'Chrome'])
        && !isSelectedMultiple(Math.random() * 3 + 1, selectedMons[name])
        && dontOfferThese(name, ['Kyurem', 'Darkrai', 'Giratina', 'Miraidon', 'Koraidon', 'Zekrom', 'Chi-yu', 'Chien-pao', 'Rayquaza']);
      // ********************************
      if (condition) {
        pokemon.click();
        clickCounter += 1;
      }
    });
  };

  const forward = (page = 999) => {
    try {
      if (document.querySelector('.modal-backdrop.fade')) {
        document.querySelector('.modal-backdrop.fade').remove();
      }
      const currentPage = document.querySelector('#pagesel')
        ? document.querySelector('#pagesel').textContent.trim()
        : null;
      if (
        document.querySelector('.pagenum.pagebutton i.fa-chevron-right') &&
        currentPage < page &&
        !TRIGGER
      ) {
        doDuplicatePokemons();
        if (clickCounter === TO_SELECT) TRIGGER = true;
        document
          .querySelector('.pagenum.pagebutton i.fa-chevron-right')
          .click();
      } else {
        clearInterval(goingForward);
        doDuplicatePokemons();
      }
    } catch (e) {
      clearInterval(goingForward);
    }
  };

  const goingForward = setInterval(forward, DELAY);
}
