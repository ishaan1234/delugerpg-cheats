{
  let allPokemons = {};

  let dupCounter = 0;
  let selectedDupCounter = 0;
  const POKEMONS_TO_SELECT = prompt('how many', 250);

  const checkPokemonEXP = (pokemon) => {
    const exp = Number(
      pokemon
        .querySelector('.info')
        .textContent.trim()
        .match(/Exp:\s?([\d,]+)/)[1]
        .replaceAll(',', '')
    );
    if (exp >= 1000000) return true;
    return false;
  };

  const clickPokemon = (pokemon, name) => {
    allPokemons[name].total -= 1;
    console.log(
      name,
      allPokemons[name].total,
      allPokemons[name].dups,
      allPokemons[name].ts,
      allPokemons[name].spe,
      allPokemons[name].speatk,
      allPokemons[name].spedef
    );
    // setTimeout(() => pokemon.click(), 100);
    setTimeout(() => pokemon.querySelector('.trade').click(), 100);
  };

  const getPokemons = () => {
    if (dupCounter >= POKEMONS_TO_SELECT) {
      return false;
    }
    const pokemons = document.querySelectorAll('.container > .row');
    for (const pokemon of pokemons) {
      if (dupCounter >= POKEMONS_TO_SELECT) break;
      const speEl = !!pokemon.querySelector('i.sbtn-spe');
      const atkEl = !!pokemon.querySelector('i.sbtn-atk');
      const defEl = !!pokemon.querySelector('i.sbtn-def');
      const hasAtk = !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
      const hasDef = !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
      const hasSpe = !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
      const hasAtkDef = hasAtk && defEl && !speEl;
      const hasNoStat = !hasAtk && !defEl && !speEl;
      const hasExp = checkPokemonEXP(pokemon);

      const hasSpeAtk = speEl && atkEl && !defEl;
      const hasSpeDef = speEl && defEl && !atkEl;
      const hasThreeStats = hasAtk && defEl && speEl;

      const name = pokemon.querySelector('div.name > h4').textContent.trim();

      if (
        Object.keys(allPokemons).includes(name) &&
        hasSpe &&
        !hasExp &&
        !hasThreeStats
      ) {
        allPokemons[name].total += 1;
        if (hasSpe && !hasExp) {
          dupCounter += 1;
          allPokemons[name].dups += 1;
          allPokemons[name].spe += 1;
        } else if (hasSpeAtk && !hasExp) {
          dupCounter += 1;
          allPokemons[name].dups += 1;
          allPokemons[name].speatk += 1;
        } else if (hasSpeDef && !hasExp) {
          dupCounter += 1;
          allPokemons[name].dups += 1;
          allPokemons[name].spedef += 1;
        } else if (hasThreeStats && !hasExp) {
          // dupCounter += 1;
          // allPokemons[name].dups += 1;
          allPokemons[name].ts += 1;
        }
      } else if (hasSpe && !hasExp && !hasThreeStats) {
        allPokemons[name] = {
          total: 1,
          dups: 0,
          ts: 0,
          spe: 0,
          speatk: 0,
          spedef: 0
        };
        if (hasSpe && !hasExp && !hasThreeStats) {
          allPokemons[name].spe = 1;
        } else if (hasSpeAtk && !hasExp) {
          allPokemons[name].speatk = 1;
        } else if (hasSpeDef && !hasExp) {
          allPokemons[name].spedef = 1;
        } else if (hasThreeStats && !hasExp) {
          allPokemons[name].ts = 1;
        }
      }
    }
  };

  const selectPokemons = () => {
    if (pageBackwardUrl !== document.location.href) {
      pageBackwardUrl = document.location.href;
      if (selectedDupCounter >= POKEMONS_TO_SELECT) {
        return false;
      }
      const pokemons = document.querySelectorAll('.container > .row');
      for (const pokemon of pokemons) {
        if (selectedDupCounter >= POKEMONS_TO_SELECT) break;
        const speEl = !!pokemon.querySelector('i.sbtn-spe');
        const atkEl = !!pokemon.querySelector('i.sbtn-atk');
        const defEl = !!pokemon.querySelector('i.sbtn-def');
        const hasAtk =
          !!pokemon.querySelector('i.sbtn-atk') && !(speEl || defEl);
        const hasDef =
          !!pokemon.querySelector('i.sbtn-def') && !(speEl || atkEl);
        const hasSpe =
          !!pokemon.querySelector('i.sbtn-spe') && !(defEl || atkEl);
        const hasAtkDef = hasAtk && defEl && !speEl;
        const hasNoStat = !hasAtk && !defEl && !speEl;
        const hasExp = checkPokemonEXP(pokemon);

        const hasSpeAtk = speEl && atkEl && !defEl;
        const hasSpeDef = speEl && defEl && !atkEl;
        const hasThreeStats = hasAtk && defEl && speEl;

        const name = pokemon.querySelector('div.name > h4').textContent.trim();
        if (
          Object.keys(allPokemons).includes(name) &&
          allPokemons[name].dups > 0 &&
          hasSpe &&
          !hasExp &&
          !hasThreeStats
        ) {
          if (hasSpe && !hasExp) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].spe -= 1;
            clickPokemon(pokemon, name);
          } else if (hasSpeAtk && !hasExp && allPokemons[name].spe === 0) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].speatk -= 1;
            clickPokemon(pokemon, name);
          } else if (
            hasSpeDef &&
            !hasExp &&
            allPokemons[name].spe === 0 &&
            allPokemons[name].speatk === 0
          ) {
            selectedDupCounter += 1;
            allPokemons[name].dups -= 1;
            allPokemons[name].spedef -= 1;
            clickPokemon(pokemon, name);
          }
        }
      }
    }
  };

  getPokemons();
}
