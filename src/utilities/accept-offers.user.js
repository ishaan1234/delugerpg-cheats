// ==UserScript==
// @name         Accept offers
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.delugerpg.com/trade/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=delugerpg.com
// @grant        none
// ==/UserScript==

(async function () {
  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  const view = document.querySelector('.hasoffer') || null;

  if (document.querySelector('.notify_done')) {
    document.location.href = 'https://www.delugerpg.com/trade/onlyoffers';
  } else if (view) {
    await delay(Math.random() * 1000 + 500); // waits between 500ms to 1500ms
    view.click();
  } else if (
    document.location.href.includes(
      'https://www.delugerpg.com/trade/viewoffer/'
    )
  ) {
    await delay(Math.random() * 2000 + 1000); // waits between 1000ms to 3000ms
    const acceptOffer = document.querySelector('.btn-primary');
    if (acceptOffer) {
      acceptOffer.click();
    }
  } else if (
    document.location.href.includes('https://www.delugerpg.com/trade/accept/')
  ) {
    await delay(Math.random() * 1500 + 750); // waits between 750ms to 2250ms
    const confirmed = document.querySelector('.btn-default');
    if (confirmed) {
      confirmed.click();
    }
  }
})();
