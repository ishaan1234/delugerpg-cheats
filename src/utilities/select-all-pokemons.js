{
  const CONSTANT_TIME = 300;
  let counter = 0;
  const toSelect = prompt('How many?', 100);
  let runOnceOnLastPageFlag = true;

  const removePageBlock = () => {
    if (document.querySelector('.modal-backdrop.fade'))
      document.querySelector('.modal-backdrop.fade').remove();
  };

  const fn = () => {
    const nextPageButton = document.querySelector(
      '.pagenum.pagebutton i.fa-chevron-right'
    );
    const hasNextButton = !!nextPageButton;
    const numberOfSelected = Number(
      document
        .querySelector('.btn-primary#offercount')
        .value.trim()
        .match(/\d+/)[0]
    );

    removePageBlock();

    if (
      (hasNextButton || runOnceOnLastPageFlag) &&
      numberOfSelected < toSelect
    ) {
      const row = document.querySelectorAll('.container > div');
      row.forEach((pokemon) => {
        // const has = pokemon.querySelectorAll('.istat i').length;
        // const has = pokemon.querySelectorAll('i.have').length;
        // const doesntHave = !!pokemon.querySelector('i.not');
        // if (doesntHave) {
        // setTimeout(() => {
        if (
          !pokemon.querySelector('input[name="offer[]"]').checked &&
          counter < toSelect
        ) {
          pokemon.querySelector('input[name="offer[]"]').click();
          counter += 1;
        }
        // }, SECOND / 5);
        // }
      });

      setTimeout(() => {
        try {
          counter < toSelect ? nextPageButton.click() : null;
        } catch (e) {
          runOnceOnLastPageFlag = false;
          clearInterval(looper);
        }
      }, CONSTANT_TIME * 3);
    } else if (numberOfSelected >= toSelect) {
      clearInterval(looper);
    } else if (!hasNextButton && runOnceOnLastPageFlag) {
      runOnceOnLastPageFlag = false;
      fn();
      clearInterval(looper);
    }
  };

  fn();
  const looper = setInterval(fn, CONSTANT_TIME * 6);
}
