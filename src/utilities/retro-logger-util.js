/* eslint-disable userscripts/filename-user */
/* eslint-disable userscripts/no-invalid-metadata */

////////////
// \n?\s+"([\w\s\(\)]+)": \{\n\s+"count":\s(\d+),\n\s+"hasForms":\s(\w+)\n\s+\},?
// $1,$2,$3\n
///////////

const hasPokemons = [
  'Dewpider',
  'Sunkern',
  'Sewaddle',
  'Cufant',
  'Clauncher',
  'Clobbopus',
  'Oranguru',
  'Carnivine',
  'Bruxish',
  'Bunnelby',
  'Pansage',
  'Wooloo',
  'Spheal',
  'Shelmet',
  'Indeedee',
  'Umbreon',
  'Jigglypuff',
  'Shuckle',
  'Mantyke',
  'Dracovish',
  'Stufful',
  'Volbeat',
  'Relicanth',
  'Swablu',
  'Woobat',
  'Whismur',
  'Lillipup',
  'Lileep',
  'Drifloon',
  'Glameow',
  'Oricorio (Baile Style)',
  'Absol',
  'Jolteon',
  'Timburr',
  'Illumise',
  'Druddigon',
  'Phanpy',
  'Ferroseed',
  'Psyduck',
  'Pansear',
  'Golett',
  'Ekans',
  'Machop',
  'Komala',
  'Grimer',
  'Falinks',
  'Torkoal',
  'Diglett',
  'Porygon',
  'Ditto',
  'Klefki',
  'Inkay',
  'Hoppip',
  'Mimikyu (Busted Form)',
  'Togedemaru',
  'Anorith',
  'Stunfisk',
  'Drampa',
  'Roggenrola',
  'Dunsparce',
  'Eevee',
  'Chimchar',
  'Rolycoly',
  'Chimecho',
  'Unova Cap Pikachu',
  'Wailmer',
  'Gourgeist (Large)',
  'Arctozolt',
  'Horsea'
];

let delay = 0;
let totalLinks = hasPokemons.length;

for (let i = 0; i < totalLinks; i++) {
  setTimeout(() => {
    console.log(`Opening link ${i + 1}/${totalLinks}`);
    window.open(
      `https://www.delugerpg.com/pokedex/info/${hasPokemons[i].replaceAll(
        ' ',
        '+'
      )}`
    );
  }, delay);
  delay += 4000;
}
